def f(n):
    a, b = 0, 1
    for i in range(0, n):
        a, b = b, a + b
    return a

n = int(input("Enter the number of terms: "))
for i in range (n):
	print(f(i))